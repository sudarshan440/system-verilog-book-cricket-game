class Book;
  int max_pages;
  
  function new(int max_pages);
    this.max_pages = max_pages;
    
  endfunction : new
  
  function int open();
    int temp;
    temp = $urandom_range(0,max_pages);
    if( temp %2 ) begin
      return temp+1;
    end else begin
      return temp;
    end
  endfunction : open
endclass : Book