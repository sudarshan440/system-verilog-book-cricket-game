class Game;
  
  Player players[];
  
  function new( int max_players );
    players = new[max_players];
    foreach( players[i]) begin
      players[i] = new();
    end
    players[0].setName("one");
    players[1].setName("two");
    players[2].setName("three");
    players[3].setName("four");
    players[4].setName("five");
    players[5].setName("six");
    
  endfunction : new
  
  function void startGame();
    foreach( players[i] ) begin
      players[i].play();
    end
    players.sort( item ) with ( item.score );
    $display("========================");
    $display("Displaying Player scores");
    $display("========================");
    foreach (players[i] )begin
      $display("Player : %d  %s score is %d", i, players[i].name, players[i].score);
    end
    
  endfunction : startGame
  
  
endclass : Game