// Code your testbench here
// or browse Examples
`include "BookCricketPkg.sv"
import BookCricketPkg::*;
module top;
  Game game;
  
  initial begin
    game = new(6);
    game.startGame();
  end
endmodule : top