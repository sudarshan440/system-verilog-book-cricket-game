class Player;
  int score;
  Book book;
  string name;
  
  function new();
    book = new(1000);
  endfunction : new
  
  function void setName( string name );
    this.name = name;
  endfunction : setName
  
  function int play();
    int temp_score;
    while(1)  begin
      temp_score = book.open();
      if( temp_score % 10 ) begin
        score = score + (temp_score % 10 );
      end else begin
        break;
      end
    end 
    return score;
  endfunction : play
endclass : Player